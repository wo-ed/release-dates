package com.woed.releasedates.activities

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.woed.releasedates.R
import com.woed.releasedates.entities.igdb.IGDBGame
import com.woed.releasedates.fragments.GameDescriptionFragment
import com.woed.releasedates.fragments.GameReleasesFragment
import com.woed.releasedates.fragments.ImagePagerFragment
import com.woed.releasedates.fragments.VideoThumbnailsFragment
import com.woed.releasedates.igdb.ImageSize
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_game_detail.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.jetbrains.anko.intentFor

class GameDetailActivity :
    AppCompatActivity(),
    ImagePagerFragment.OnFragmentInteractionListener {

    private val realm by lazy { Realm.getDefaultInstance() }
    private val pagerAdapter: SectionsPagerAdapter by lazy {
        SectionsPagerAdapter(
            supportFragmentManager
        )
    }
    private var gameId: Long = -1
    private var game: IGDBGame? = null

    private val imageUrls: List<String>?
        get() = game?.screenshots?.map { it.getImageUrl(ImageSize.ScreenshotHuge) }?.reversed()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_detail)

        gameId = intent.getLongExtra(intentKeyGameId, -1)
        game = realm.where(IGDBGame::class.java).equalTo("id", gameId).findFirst()

        supportActionBar?.apply {
            subtitle = game?.name
            elevation = 0F
        }

        viewPager.adapter = pagerAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        realm.close()
    }

    override fun onFullScreenButtonClick(position: Int) {
        imageUrls?.let { ImagePagerActivity.start(this, it, position) }
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int {
            return 4
        }

        override fun getItem(position: Int): Fragment {
            val imagesCount = game?.screenshots?.size ?: 0
            val imageIndex = if (imagesCount > 0) position % imagesCount else null

            // TODO
            val imageUrl = if (imageIndex != null && imageIndex >= 0) {
                game?.screenshots?.get(imageIndex)?.getImageUrl(ImageSize.ScreenshotHuge)
            } else {
                game?.cover?.getImageUrl(ImageSize.CoverBig)
            }

            return when (position) {
                0 -> GameReleasesFragment.newInstance(gameId, imageUrl)
                1 -> GameDescriptionFragment.newInstance(
                    game?.summary ?: getString(R.string.no_description), imageUrl
                )
                2 -> ImagePagerFragment.newInstance(imageUrls ?: emptyList())
                3 -> VideoThumbnailsFragment.newInstance(gameId)
                else -> throw NoSuchElementException()
            }
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> getString(R.string.release_dates)
                1 -> getString(R.string.description)
                2 -> getString(R.string.screenshots)
                3 -> getString(R.string.videos)
                else -> throw NoSuchElementException()
            }
        }
    }

    companion object {
        val intentKeyGameId = "gameId"

        fun start(context: Context, gameId: Long) {
            context.startActivity(
                context.intentFor<GameDetailActivity>(
                    intentKeyGameId to gameId
                )
            )
        }
    }
}