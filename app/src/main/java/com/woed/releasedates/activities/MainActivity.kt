package com.woed.releasedates.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.woed.releasedates.R
import com.woed.releasedates.entities.Bookmark
import com.woed.releasedates.entities.igdb.IGDBGame
import com.woed.releasedates.fragments.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.jetbrains.anko.AnkoLogger

class MainActivity : AppCompatActivity(), AnkoLogger,
    NavigationView.OnNavigationItemSelectedListener,
    BookmarksFragment.OnFragmentInteractionListener,
    BrowseGamesFragment.OnFragmentInteractionListener {

    private var mainContent: MainContent? = null
        private set(value) {
            field = value
            field?.let { f ->
                title = f.title ?: getString(R.string.app_name)
                f.menuItemId?.let { navView?.menu?.findItem(it)?.isChecked = true }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        viewStubPageContent.apply {
            layoutResource = R.layout.content_main
            inflate()
        }

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        if (savedInstanceState == null) {
            replaceMainContentFragment(StoredDatesMCFragment.newInstance(), false)
        } else {
            title = savedInstanceState.getString(bundleKeyTitle)
        }

        supportFragmentManager.addOnBackStackChangedListener {
            mainContent = supportFragmentManager.findFragmentById(R.id.contentMain) as? MainContent
        }
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navBrowseDates -> {
                replaceMainContentFragment(BrowseReleaseDatesMCFragment.newInstance(), false)
            }
            R.id.navMyDates ->
                replaceMainContentFragment(StoredDatesMCFragment.newInstance(), false)
        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun <F> replaceMainContentFragment(fragment: F, addToBackStack: Boolean)
            where F : Fragment, F : MainContent {

        val transaction = supportFragmentManager.beginTransaction()
            .replace(R.id.contentMain, fragment)

        if (addToBackStack) {
            transaction.addToBackStack(null)
        }
        transaction.commit()
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        if (fragment is MainContent) {
            mainContent = fragment
        }
    }

    override fun onAddGameClick() {
        replaceMainContentFragment(BrowseReleaseDatesMCFragment.newInstance(), false)
    }

    override fun onBookmarkClick(bookmark: Bookmark) {
        bookmark.game?.id?.let { GameDetailActivity.start(this, it) }
    }

    override fun onGameClick(game: IGDBGame) {
        game.id?.let { GameDetailActivity.start(this, it) }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(bundleKeyTitle, title.toString())
    }

    companion object {
        val bundleKeyTitle = "title"
    }
}
