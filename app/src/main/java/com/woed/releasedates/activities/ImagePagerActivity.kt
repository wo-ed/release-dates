package com.woed.releasedates.activities

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.woed.releasedates.R
import com.woed.releasedates.adapters.ImagePagerAdapter
import kotlinx.android.synthetic.main.activity_image_pager.*
import org.jetbrains.anko.intentFor

class ImagePagerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_pager)

        val imageUrls = intent.getStringArrayListExtra(intentKeyImageUrls)

        imageUrls?.let {
            imagePager.apply {
                adapter = ImagePagerAdapter(context, it)
                currentItem = intent.getIntExtra(intentKeyPosition, 0)
            }
        }

        @SuppressLint("InlinedApi")
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    }

    companion object {
        val intentKeyImageUrls = "imageUrls"
        val intentKeyPosition = "position"

        fun start(context: Context, imageUrls: Iterable<String>, position: Int) {
            context.startActivity(
                context.intentFor<ImagePagerActivity>(
                    intentKeyImageUrls to imageUrls,
                    intentKeyPosition to position
                )
            )
        }
    }
}