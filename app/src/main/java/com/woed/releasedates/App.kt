package com.woed.releasedates

import android.app.Application
import com.woed.releasedates.receivers.AlarmReceiver
import io.realm.Realm
import net.danlew.android.joda.JodaTimeAndroid

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        JodaTimeAndroid.init(this)
        AlarmReceiver.setupAlarm(this)
    }
}

