package com.woed.releasedates.entities.igdb

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.lang.reflect.Type

open class IGDBGame : RealmObject() {
    @PrimaryKey
    var id: Long? = null

    @SerializedName("release_dates")
    var releases: RealmList<IGDBRelease>? = null

    var videos: RealmList<IGDBVideo>? = null
    var screenshots: RealmList<IGDBImage>? = null
    var cover: IGDBImage? = null
    var name: String? = null
    var summary: String? = null

    val firstDate get() = releases?.firstOrNull()?.date

    class ListDeserializer : ResponseDeserializable<List<IGDBGame>> {
        override fun deserialize(content: String): List<IGDBGame> =
            Gson().fromJson(content, listType)
    }

    fun deleteFromRealmWithReferences() {
        releases?.deleteAllFromRealm()
        screenshots?.deleteAllFromRealm()
        videos?.deleteAllFromRealm()
        cover?.deleteFromRealm()
        deleteFromRealm()
    }

    companion object {
        private val listType: Type = object : TypeToken<List<IGDBGame>>() {}.type
    }
}

