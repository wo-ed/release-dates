package com.woed.releasedates.entities.igdb

import android.content.Context
import com.google.gson.annotations.SerializedName
import com.woed.releasedates.R
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Days
import org.joda.time.format.DateTimeFormat

open class IGDBRelease : RealmObject() {
    @PrimaryKey
    var id: Long? = null

    @SerializedName("platform")
    var platformId: Long? = null

    @SerializedName("region")
    var regionId: Byte? = null

    var date: Long? = null

    val region get() = Region.values().find { it.apiRegion == regionId }

    fun getRegionText(context: Context) = region?.stringResource?.let { context.getString(it) }

    fun getDateText(context: Context): String? {
        val date = this.date

        return if (date != null && date > 0) {
            val releaseDate = DateTime(date, DateTimeZone.UTC)

            if (releaseDate.monthOfYear == 12 && releaseDate.dayOfMonth == 31) {
                releaseDate.year.toString()
            } else {

                val today = DateTime.now().withMillisOfDay(0)
                val days = Days.daysBetween(today, releaseDate).days

                var text = releaseDate.toString(dateTimeFormatter)
                if (days > 0) {
                    text += " ${context.resources.getQuantityString(R.plurals.days, days, days)}"
                } else if (days == 0) {
                    text += " ${context.getString(R.string.today)}"
                }
                text
            }
        } else {
            null
        }
    }

    enum class Region(val apiRegion: Byte, val stringResource: Int) {
        Europe(1, R.string.europe),
        North_America(2, R.string.north_america),
        Australia(3, R.string.australia),
        New_Zealand(4, R.string.new_zealand),
        Japan(5, R.string.japan),
        China(6, R.string.china),
        Asia(7, R.string.asia),
        Worldwide(8, R.string.worldwide)
    }

    companion object {
        private val dateTimeFormatter = DateTimeFormat.forPattern("dd.MM.yyyy")
    }
}