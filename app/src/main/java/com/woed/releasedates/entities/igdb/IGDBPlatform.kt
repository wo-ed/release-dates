package com.woed.releasedates.entities.igdb

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.lang.reflect.Type

open class IGDBPlatform : RealmObject() {
    @PrimaryKey
    var id: Long? = null

    var logo: IGDBImage? = null
    var name: String? = null

    fun deleteFromRealmWithReferences() {
        logo?.deleteFromRealm()
        deleteFromRealm()
    }

    class ListDeserializer : ResponseDeserializable<List<IGDBPlatform>> {
        override fun deserialize(content: String): List<IGDBPlatform>? {
            return Gson().fromJson(content, listType)
        }
    }

    companion object {
        private val listType: Type = object : TypeToken<List<IGDBPlatform>>() {}.type
    }
}
