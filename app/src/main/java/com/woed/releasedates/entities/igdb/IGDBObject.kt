package com.woed.releasedates.entities.igdb

import com.google.gson.annotations.SerializedName
import com.woed.releasedates.igdb.ImageSize
import com.woed.releasedates.igdb.imageUrlTemplate
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class IGDBImage : RealmObject() {
    @PrimaryKey
    var id: Long? = null

    @SerializedName("image_id")
    var imageId: String? = null

    fun getImageUrl(size: ImageSize): String {
        return imageUrlTemplate.format(size.value, imageId)
    }
}

open class IGDBVideo : RealmObject() {
    @PrimaryKey
    var id: Long? = null

    @SerializedName("video_id")
    var videoId: String? = null

    var name: String? = null
}