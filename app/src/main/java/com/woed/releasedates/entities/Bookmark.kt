package com.woed.releasedates.entities

import com.woed.releasedates.entities.igdb.IGDBGame
import com.woed.releasedates.entities.igdb.IGDBRelease
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Bookmark : RealmObject() {
    @PrimaryKey
    var id = UUID.randomUUID().toString()

    var release: IGDBRelease? = null
    var game: IGDBGame? = null
}