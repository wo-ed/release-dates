package com.woed.releasedates.fragments

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.bignerdranch.android.multiselector.MultiSelector
import com.woed.releasedates.R
import com.woed.releasedates.adapters.BookmarkRealmAdapter
import com.woed.releasedates.adapters.BookmarkViewHolder
import com.woed.releasedates.entities.Bookmark
import com.woed.releasedates.utils.LayoutManagerFactory
import com.woed.releasedates.utils.NoItemsRealmListener
import com.woed.releasedates.utils.getContextColor
import com.woed.releasedates.utils.setIconsTint
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_bookmarks.*

open class BookmarksFragment : Fragment() {
    private val realm by lazy { Realm.getDefaultInstance() }
    private val actionModeCallback by lazy { ActionModeCallback() }

    private val multiSelector by lazy {
        MultiSelector().apply { isSelectable = true }
    }

    private val bookmarks by lazy {
        realm.where(Bookmark::class.java).findAllAsync()
    }

    private val bookmarkAdapter by lazy {
        context?.let { BookmarkRealmAdapter(it, bookmarks, true, multiSelector, ClickListener()) }
    }

    private val changeListener by lazy {
        NoItemsRealmListener<RealmResults<Bookmark>>(textViewNoGames)
    }

    private var actionMode: ActionMode? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        savedInstanceState?.let {
            if (it.getBoolean(bundleKeyActionModeStarted)) {
                actionMode = activity?.startActionMode(actionModeCallback)
            }
            multiSelector.restoreSelectionStates(it.getBundle(bundleKeySelectionStates))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_bookmarks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewGames.apply {
            adapter = bookmarkAdapter
            layoutManager = LayoutManagerFactory.createLinearLayoutManger(context)
        }

        bookmarks.addChangeListener(changeListener)

        fab.setOnClickListener { listener?.onAddGameClick() }
    }

    override fun onStop() {
        super.onStop()
        actionMode?.finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        bookmarks.removeChangeListener(changeListener)
        realm.close()
    }

    private fun getCheckedBookmarks(): List<Bookmark> = multiSelector.selectedPositions.mapNotNull {
        bookmarkAdapter?.getItem(it)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.apply {
            putBoolean(bundleKeyActionModeStarted, actionMode != null)
            putBundle(bundleKeySelectionStates, multiSelector.saveSelectionStates())
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? OnFragmentInteractionListener
                ?: throw RuntimeException("$context must implement ${OnFragmentInteractionListener::class}")
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private inner class ClickListener : BookmarkViewHolder.ClickListener {

        override fun onLongClick(holder: BookmarkViewHolder): Boolean {
            actionMode = actionMode ?: activity?.startActionMode(actionModeCallback)
            toggleSelection(holder)
            return true
        }

        override fun onClick(holder: BookmarkViewHolder) {
            if (actionMode != null) {
                toggleSelection(holder)
            } else {
                holder.bookmark?.let { listener?.onBookmarkClick(it) }
            }
        }

        private fun toggleSelection(holder: BookmarkViewHolder) {
            multiSelector.tapSelection(holder)
            finishActionModeIfNoItemChecked()
        }

        private fun finishActionModeIfNoItemChecked() {
            if (multiSelector.selectedPositions.size == 0) {
                actionMode?.finish()
            }
        }
    }

    private inner class ActionModeCallback : ActionMode.Callback {

        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            mode.menuInflater.inflate(R.menu.menu_stored_dates, menu)
            getContextColor(R.color.colorToolbarIconTint)?.let { menu.setIconsTint(it) }
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu) = false

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem) =
            when (item.itemId) {
                R.id.itemDelete -> {
                    removeCheckedGames()
                    mode.finish()
                    true
                }
                else -> false
            }

        override fun onDestroyActionMode(mode: ActionMode) {
            actionMode = null
            multiSelector.clearSelections()
        }

        private fun removeCheckedGames() {
            getCheckedBookmarks().forEach { bookmark ->
                realm.executeTransaction { bookmark.deleteFromRealm() }
            }
        }
    }

    interface OnFragmentInteractionListener {
        fun onAddGameClick()
        fun onBookmarkClick(bookmark: Bookmark)
    }

    companion object {
        private val bundleKeyActionModeStarted = "actionModeStarted"
        private val bundleKeySelectionStates = "selectionStates"

        fun newInstance() = BookmarksFragment()
    }
}
