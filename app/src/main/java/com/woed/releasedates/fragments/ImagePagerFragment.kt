package com.woed.releasedates.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.woed.releasedates.R
import com.woed.releasedates.adapters.ImagePagerAdapter
import kotlinx.android.synthetic.main.fragment_image_pager.*
import org.jetbrains.anko.onClick
import java.util.*

class ImagePagerFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null

    fun init(imageUrls: Collection<String>) {
        arguments = Bundle().apply {
            putStringArrayList(bundleKeyImageUrls, ArrayList<String>(imageUrls))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_image_pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getStringArrayList(bundleKeyImageUrls)?.let { imageUrls ->
            imagePager.adapter = context?.let { ImagePagerAdapter(it, imageUrls) }
        }

        buttonFullscreen.onClick { listener?.onFullScreenButtonClick(imagePager.currentItem) }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? OnFragmentInteractionListener
            ?: throw RuntimeException("$context must implement ${OnFragmentInteractionListener::class}")
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFullScreenButtonClick(position: Int)
    }

    companion object {
        private val bundleKeyImageUrls = "imageUrls"

        fun newInstance(imageUrls: Collection<String>): ImagePagerFragment {
            return ImagePagerFragment().apply {
                init(imageUrls)
            }
        }
    }
}