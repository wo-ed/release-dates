package com.woed.releasedates.fragments

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.woed.releasedates.R
import com.woed.releasedates.adapters.IGDBGameAdapter
import com.woed.releasedates.adapters.IGDBGameViewHolder
import com.woed.releasedates.entities.igdb.IGDBGame
import com.woed.releasedates.igdb.IGDB
import com.woed.releasedates.igdb.getGames
import com.woed.releasedates.utils.LayoutManagerFactory
import com.woed.releasedates.utils.setIconsTint
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_search_games.*
import kotlinx.coroutines.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.inputMethodManager
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

open class BrowseGamesFragment : Fragment(), AnkoLogger {
    private val realm by lazy { Realm.getDefaultInstance() }
    private val games by lazy { arrayListOf<IGDBGame>() }
    private val gson by lazy { Gson() }
    private var listener: OnFragmentInteractionListener? = null
    private var query: String = ""

    private var job = Job()
    private val scopeMainThread = CoroutineScope(job + Dispatchers.Main)
    private val scopeIO = CoroutineScope(job + Dispatchers.IO)


    private val gamesAdapter by lazy {
        context?.let { IGDBGameAdapter(it, games, ClickListener()) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        this.query = savedInstanceState?.getString(bundleKeyQuery) ?: ""
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_games, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewResults.apply {
            adapter = gamesAdapter
            layoutManager = LayoutManagerFactory.createLinearLayoutManger(context)
        }

        savedInstanceState?.let {
            val games = it.getStringArrayList(bundleKeyGames)
                ?.map { game -> gson.fromJson(game, IGDBGame::class.java) }
            if (games != null) {
                initializeGames(games)
            }
        }

        context?.let {
            spinnerSortGames.adapter = ArrayAdapter.createFromResource(
                it,
                R.array.sort_games, android.R.layout.simple_spinner_item
            ).apply {

                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }
        }

        // TODO: stop onItemSelected from firing immediately
        spinnerSortGames.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                performSearch()
            }
        }

        //TODO: load more if not less than 50
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.browse_dates, menu)
        context?.let {
            menu.setIconsTint(getColor(it, R.color.colorToolbarIconTint))
        }

        val searchItem = menu.findItem(R.id.actionSearch)

        val searchView = (searchItem?.actionView as? SearchView)?.apply {
            isIconified = true
            queryHint = getString(R.string.search)
            setQuery(this@BrowseGamesFragment.query, false)
        }

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                this@BrowseGamesFragment.query = query
                performSearch()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                this@BrowseGamesFragment.query = newText
                subAppBarLayout.setExpanded(true)
                return true
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? OnFragmentInteractionListener
            ?: throw RuntimeException("$context must implement ${OnFragmentInteractionListener::class}")
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onStop() {
        super.onStop()
        hideKeyboard()
    }

    override fun onDestroy() {
        super.onDestroy()
        realm.close()
        job.cancel()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putStringArrayList(bundleKeyGames, ArrayList(games.map { gson.toJson(it) }))
        outState.putString(bundleKeyQuery, query)
    }

    private fun performSearch() {
        val text = query

        clearGames()
        hideKeyboard()
        search(text)
    }

    private fun hideKeyboard() {
        activity?.currentFocus?.let {
            context?.inputMethodManager?.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    private fun search(text: String) {
        progressBar.visibility = View.VISIBLE

/*        val parameters = mutableListOf(
            "fields" to IGDB.fieldsGame,
            "limit" to "50",
            "offset" to "0"
        )

        if (query.isNotBlank()) {
            parameters += "search" to text
        }

        // TODO: once ordering is not match, there will not be searched by text
        val orderTitle =
            resources.getStringArray(R.array.sort_games)[spinnerSortGames.selectedItemPosition]
        val order = when (orderTitle) {
            getString(R.string.order_match) -> null
            getString(R.string.order_name) -> "name"
            getString(R.string.order_date) -> "release_dates.date:asc:max"
            else -> null
        }

        order?.let { parameters += "order" to it }

        // TODO: games without date are ignored
        parameters += "filter[release_dates.date][gte]" to
                DateTime.now(DateTimeZone.UTC).millis.toString()*/

        scopeIO.launch {
            val (_, _, result) = getGames()
            val (games, err) = result

            scopeMainThread.launch {
                err?.let { error("Unable to retrieve games from server", it) }
                games?.let { initializeGames(it) }
                progressBar.visibility = View.GONE
            }
        }
    }

    private fun clearGames() {
        games.clear()
        gamesAdapter?.notifyDataSetChanged()
    }

    private fun initializeGames(games: Iterable<IGDBGame>) {
        this.games.clear()
        this.games += games
        gamesAdapter?.notifyDataSetChanged()
    }

    interface OnFragmentInteractionListener {
        fun onGameClick(game: IGDBGame)
    }

    inner class ClickListener : IGDBGameViewHolder.ClickListener {

        override fun onClick(holder: IGDBGameViewHolder) {
            holder.game?.let { game ->
                realm.executeTransaction { realm.copyToRealmOrUpdate(game) }
                listener?.onGameClick(game)
            }
        }
    }

    companion object {
        private val bundleKeyGames = "games"
        private val bundleKeyQuery = "query"

        fun newInstance() = BrowseGamesFragment()
    }
}
