package com.woed.releasedates.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.woed.releasedates.R
import com.woed.releasedates.utils.loadUrl
import kotlinx.android.synthetic.main.fragment_game_description.*

class GameDescriptionFragment : Fragment() {

    fun init(description: String, imageUrl: String?) {
        arguments = Bundle().apply {
            putString(bundleKeyDescription, description)
            putString(bundleKeyImageUrl, imageUrl)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_game_description, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textViewGameDescription.text = arguments?.getString(bundleKeyDescription)

        val imageUrl = arguments?.getString(bundleKeyImageUrl)
        imageUrl?.let { imageBackground.loadUrl(it) }
    }

    companion object {
        val bundleKeyDescription = "bundleKeyDescription"
        val bundleKeyImageUrl = "imageUrl"

        fun newInstance(description: String, imageUrl: String?) =
            GameDescriptionFragment().apply { init(description, imageUrl) }
    }
}