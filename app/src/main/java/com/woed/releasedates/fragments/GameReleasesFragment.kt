package com.woed.releasedates.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bignerdranch.android.multiselector.MultiSelector
import com.woed.releasedates.R
import com.woed.releasedates.adapters.IGDBReleaseRealmAdapter
import com.woed.releasedates.adapters.IGDBReleaseViewHolder
import com.woed.releasedates.entities.Bookmark
import com.woed.releasedates.entities.igdb.IGDBGame
import com.woed.releasedates.entities.igdb.IGDBRelease
import com.woed.releasedates.utils.NoItemsRealmListener
import com.woed.releasedates.utils.VerticalDividerItemDecoration
import com.woed.releasedates.utils.loadUrl
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmResults
import io.realm.Sort
import kotlinx.android.synthetic.main.fragment_game_releases.*

open class GameReleasesFragment : Fragment() {
    private var gameId: Long? = null
    private val realm by lazy { Realm.getDefaultInstance() }

    val game: IGDBGame? by lazy {
        realm.where(IGDBGame::class.java).equalTo("id", gameId).findFirst()
    }

    private val releases by lazy {
        game?.releases?.sort("platformId", Sort.ASCENDING, "regionId", Sort.ASCENDING)
    }

    private val changeListener by lazy {
        NoItemsRealmListener<RealmResults<IGDBRelease>>(textViewNoItems)
    }

    private val releasesAdapter by lazy {
        releases?.let {
            context?.let { ctx ->
                IGDBReleaseRealmAdapter(
                    ctx,
                    it,
                    true,
                    ClickListener(),
                    multiSelector
                )
            }
        }
    }

    private val multiSelector by lazy {
        MultiSelector().apply { isSelectable = true }
    }

    fun init(gameId: Long?, imageUrl: String?) {
        arguments = Bundle().apply {
            gameId?.let { putLong(bundleKeyGameId, it) }
            putString(bundleKeyImageUrl, imageUrl)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        gameId = arguments?.getLong(bundleKeyGameId)

        loadChecks()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_game_releases, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        val dividerItemDecoration =
            context?.let { ctx ->
                ContextCompat.getDrawable(ctx, R.drawable.divider)?.let {
                    VerticalDividerItemDecoration(
                        it
                    )
                }
            }

        releases?.addChangeListener(changeListener)

        recyclerViewReleases.apply {
            adapter = releasesAdapter
            this.layoutManager = layoutManager
            dividerItemDecoration?.let { addItemDecoration(it) }
        }

        val imageUrl = arguments?.getString(bundleKeyImageUrl)
        imageUrl?.let { imageBackground.loadUrl(it) }
    }

    private fun loadChecks() {
        val bookmarks = releases?.map {
            realm.where(Bookmark::class.java).equalTo("release.id", it.id).findFirst()
        }

        bookmarks?.map { it != null }
            ?.forEachIndexed { index, isBookmarked ->
                val id = releasesAdapter?.getItemId(index)
                id?.let { multiSelector.setSelected(index, it, isBookmarked) }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        releases?.removeChangeListener(changeListener)
        realm.close()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        gameId?.let { outState.putLong(bundleKeyGameId, it) }
    }

    private inner class ClickListener : IGDBReleaseViewHolder.ClickListener {
        override fun onClick(holder: IGDBReleaseViewHolder) {
            holder.release?.let {
                multiSelector.tapSelection(holder)
                toggleGameStored(it)
            }
        }
    }

    private fun toggleGameStored(inputRelease: IGDBRelease) {
        val bookmark =
            realm.where(Bookmark::class.java).equalTo("release.id", inputRelease.id).findFirst()

        realm.executeTransaction {
            if (bookmark != null) {
                bookmark.deleteFromRealm()
            } else {
                val newBookmark = Bookmark().apply {
                    release = inputRelease
                    game = this@GameReleasesFragment.game
                }
                realm.copyToRealmOrUpdate(newBookmark)
            }
        }
    }

    companion object {
        private const val bundleKeyGameId = "gameId"
        private const val bundleKeyImageUrl = "imageUrl"

        fun newInstance(gameId: Long, imageUrl: String?) =
            GameReleasesFragment().apply { init(gameId, imageUrl) }
    }
}