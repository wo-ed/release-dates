package com.woed.releasedates.fragments

import com.woed.releasedates.R

interface MainContent {
    val title: String?
    val menuItemId: Int?
}

class BrowseReleaseDatesMCFragment : BrowseGamesFragment(), MainContent {
    override val title: String? get() = getString(R.string.browse_dates)
    override val menuItemId: Int? = R.id.navBrowseDates

    companion object {
        fun newInstance() = BrowseReleaseDatesMCFragment()
    }
}

class StoredDatesMCFragment : BookmarksFragment(), MainContent {
    override val title: String? get() = getString(R.string.my_games)
    override val menuItemId: Int? = R.id.navMyDates

    companion object {
        fun newInstance() = StoredDatesMCFragment()
    }
}