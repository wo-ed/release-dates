package com.woed.releasedates.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.woed.releasedates.R
import com.woed.releasedates.adapters.IGDBVideoAdapter
import com.woed.releasedates.adapters.IGDBVideoViewHolder
import com.woed.releasedates.entities.igdb.IGDBGame
import com.woed.releasedates.utils.LayoutManagerFactory
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_video_thumbnails.*
import org.jetbrains.anko.AnkoLogger

class VideoThumbnailsFragment : Fragment(), AnkoLogger {

    private val realm by lazy { Realm.getDefaultInstance() }

    private val game by lazy {
        val gameId = arguments?.getLong(bundleKeyGameId)
        realm.where(IGDBGame::class.java).equalTo("id", gameId).findFirst()
    }

    private val videoAdapter by lazy {
        game?.videos?.let { context?.let { ctx -> IGDBVideoAdapter(ctx, it, ClickListener()) } }
    }

    fun init(gameId: Long) {
        arguments = Bundle().apply {
            putLong(bundleKeyGameId, gameId)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_video_thumbnails, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewThumbnails.apply {
            adapter = videoAdapter
            layoutManager = LayoutManagerFactory.createLinearLayoutManger(context)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        realm.close()
    }

    private inner class ClickListener : IGDBVideoViewHolder.ClickListener {
        override fun onClick(holder: IGDBVideoViewHolder) {
            holder.video?.let {
                val videoId = it.id

                //TODO
                /*
                val intent = YouTubeStandalonePlayer.createVideoIntent(
                        activity,
                        "AIzaSyAXeFOoH9V_tbkQIpmmvJ4qr9Au8v76P6k",
                        videoId)
                try {
                    startActivity(intent)
                } catch(ex: ActivityNotFoundException) {
                    error("YouTube activity could not be started", ex)
                    context.browse("https://www.youtube.com/watch?v=$videoId")
                }*/
            }
        }
    }

    companion object {
        private val bundleKeyGameId = "gameId"

        fun newInstance(gameId: Long): VideoThumbnailsFragment {
            return VideoThumbnailsFragment().apply {
                init(gameId)
            }
        }
    }
}