package com.woed.releasedates.igdb

import com.github.kittinunf.fuel.httpPost
import com.woed.releasedates.entities.igdb.IGDBGame

object IGDB {
    val userKey = "user-key" to "d47a34c0eab7555b614fc627787d29da"
    val baseUrl = "https://api-v3.igdb.com/"
    val gamesUrl = baseUrl + "games/"
    val platformsUrl = baseUrl + "platforms/"
    val gameUrl = "${IGDB.gamesUrl}%d/"
    val fieldsGame = "id,name,cover,release_dates,screenshots,summary,videos"
    val fieldsPlatform = "id,name,logo"

    val filteredGamesUrl = baseUrl + "games/?search=%s&filter[rating][gte]=80&filter[release_dates.date][gt]=YYYY-MM-DD"
}

fun getGames() = IGDB.gamesUrl.httpPost()
    .header(IGDB.userKey, "Accept" to "application/json")
    .body("fields release_dates.date,videos.name,videos.video_id,screenshots.image_id,cover.image_id,name,summary; sort release_dates.date asc; limit 50; offset 0;")
    .responseObject(IGDBGame.ListDeserializer())

class UrlBuilder {

    private var endpoint = ""
    private var search = ""
    private val filters = mutableMapOf<String, MutableMap<String, String>>()

    fun endpoint(endpoint: String) {
        this.endpoint = endpoint
    }

    fun search(search: String) {
        this.search = search
    }

    fun filter(filter: Triple<String, String, String>) {
        val (field, postfix, value) = filter
        val map = filters.getOrPut(field) { mutableMapOf<String, String>() }
        map[postfix] = value
    }

    fun filters(vararg filters: Triple<String, String, String>) {
        filters.forEach { filter(it) }
    }

    private val filtersString: () -> String
        get() = {
            val triples = filters.flatMap { outerEntry ->
                outerEntry.value.map {
                    innerEntry ->
                    Triple(outerEntry.key, innerEntry.key, innerEntry.value)
                }
            }

            triples.map { (field, postfix, value) ->
                "filter[$field][$postfix]=$value"
            }.joinToString("&")
        }

    override fun toString(): String {
        return "${IGDB.baseUrl}$endpoint/?search=$search&$filtersString"
    }
}

enum class ImageSize(val value: String) {
    CoverSmall("cover_small"), // 90 x 128 Fit
    ScreenshotMed("screenshot_med"), // 569 x 320 Lfill, Center gravity
    CoverBig("cover_big"), // 227 x 320 Fit
    LogoMed("logo_med"), // 284 x 160 Fit
    ScreenshotBig("screenshot_big"), // 889 x 500 Lfill, Center gravity
    ScreenshotHuge("screenshot_huge"), // 1280 x 720 Lfill, Center gravity
    Thumb("thumb"), // 90 x 90 Thumb, Center gravity
    Micro("micro") // 35 x 35 Thumb, Center gravity
}

const val imageUrlTemplate = "https://images.igdb.com/igdb/image/upload/t_%s/%s.jpg";