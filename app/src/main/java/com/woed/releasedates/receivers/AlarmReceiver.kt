package com.woed.releasedates.receivers

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.woed.releasedates.R
import com.woed.releasedates.activities.MainActivity
import com.woed.releasedates.entities.igdb.IGDBGame
import org.jetbrains.anko.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

class AlarmReceiver : BroadcastReceiver(), AnkoLogger {

    override fun onReceive(context: Context, intent: Intent) {
        info("Broadcast received: action = ${intent.action}")

        when (intent.action) {
            "android.intent.action.BOOT_COMPLETED",
            "android.intent.action.QUICKBOOT_POWERON" -> {
                // setUpAlarm does not have to be called since it is called in App.onCreate anyways
                // setUpAlarm(context)
            }
            /*
            actionAlarmSync ->
                synchronize(context)*/
        }
    }

    /*
    //TODO: ignore very old dates
    //TODO: reminder setting in db
    //TODO: notify for delay
    //TODO: sync platforms before app can be used
    private fun synchronize(context: Context) = async {
        Realm.getDefaultInstance().use { realm ->

            val requestLimit = 50
            val count = (0 .. 200 step requestLimit).map { offset ->

                val p = listOf(
                        "fields" to IGDB.fieldsPlatform,
                        "limit" to requestLimit,
                        "offset" to offset)

                val (_, _, result) = await {
                    IGDB.platformsUrl
                            .httpGet(p)
                            .header(IGDB.authorization)
                            .responseObject(IGDBPlatform.ListDeserializer())
                }

                val (platforms, err) = result
                err?.let { error("Unable to retrieve platforms from server", it) }
                if (platforms != null) {
                    var count = 0
                    //TODO: async
                    realm.executeTransaction {
                        count = realm.copyToRealmOrUpdate(platforms).size
                    }
                    count
                } else {
                    0
                }
            }.sum()

            info("$count platforms stored")

            //TODO: async
            val bookmarks = realm.where(Bookmark::class.java).findAll()
            val games = bookmarks.mapNotNull { it.game }

            val parameters = listOf("fields" to IGDB.fieldsGame)

            val updates = games.map { it.id }.mapNotNull { id ->
                val (_, _, result) = await {
                    IGDB.gameUrl.format(id)
                            .httpGet(parameters)
                            .header(IGDB.authorization)
                            .responseObject(IGDBGame.ListDeserializer())
                }

                val (retrievedGames, err) = result

                // The list should contain only one element
                val retrievedGame = retrievedGames?.firstOrNull()

                err?.let { error("Unable to retrieve game $id from server", it) }

                retrievedGame
            }

            //TODO: async
            realm.executeTransaction { realm.copyToRealmOrUpdate(updates) }

            showReleasesNotification(context, games)
        }
        //TODO: ask for delete/archive inside BookmarksFragment
    }*/

    companion object : AnkoLogger {
        val actionAlarmSync = "alarmSync"

        val requestAlarmSync = 0
        val requestReleasesNotification = 1

        val notificationIdNewReleases = 0

        fun setupAlarm(context: Context) {

            val intent = context.intentFor<AlarmReceiver>().apply {
                action = actionAlarmSync
            }

            val pendingIntent = PendingIntent.getBroadcast(
                    context,
                    requestAlarmSync,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT)

            context.alarmManager.setInexactRepeating(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(),
                    AlarmManager.INTERVAL_DAY,
                    pendingIntent)
        }

        private fun showReleasesNotification(context: Context, games: Iterable<IGDBGame>) {
            val nowUtc = DateTime.now(DateTimeZone.UTC)

            val filteredGames = games.filter { game ->
                val releaseDay = game.firstDate?.let {
                    DateTime(it, DateTimeZone.UTC).withMillisOfDay(0)
                }
                releaseDay != null && releaseDay <= nowUtc
            }

            if (filteredGames.isNotEmpty()) {
                val title = context.resources.getQuantityString(R.plurals.new_games_released,
                        filteredGames.size, filteredGames.size)
                val text = filteredGames.map { it.name }.joinToString(", ")
                showNotification(title, text, notificationIdNewReleases, context)
            }
        }

        private fun showNotification(title: String, text: String, id: Int, context: Context) {
            val notificationBuilder = NotificationCompat.Builder(context)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setSmallIcon(R.drawable.ic_gamepad_black_24dp)

            val resultIntent = context.intentFor<MainActivity>()

            // The stack builder object will contain an artificial back stack for the
            // started Activity.
            // This ensures that navigating backward from the Activity leads out of
            // your application to the Home screen.
            val stackBuilder = TaskStackBuilder.create(context)
            // Adds the back stack for the Intent (but not the Intent itself)
            //stackBuilder.addParentStack(MainActivity::class.java)
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                    requestReleasesNotification,
                    PendingIntent.FLAG_UPDATE_CURRENT)
            notificationBuilder.setContentIntent(resultPendingIntent)
            // id allows you to update the notification later on.
            context.notificationManager.notify(id, notificationBuilder.build())
        }
    }
}
