package com.woed.releasedates.utils

import android.view.View
import io.realm.RealmChangeListener
import io.realm.RealmResults
import java.lang.ref.WeakReference

class NoItemsRealmListener<T>(view: View) : RealmChangeListener<T> where T : RealmResults<*> {
    private val view: WeakReference<View> = WeakReference(view)

    override fun onChange(releases: T) {
        val itemsCount = releases.size
        view.get()?.visibility = if (itemsCount == 0) View.VISIBLE else View.GONE
    }
}