package com.woed.releasedates.utils

import android.content.Context
import android.content.res.Configuration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

object LayoutManagerFactory {
    fun createLinearLayoutManger(context: Context): LinearLayoutManager {
        return if (context.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            GridLayoutManager(context, 3)
        } else {
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
    }
}