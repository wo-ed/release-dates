package com.woed.releasedates.utils

import android.content.ContentValues
import android.content.Context
import android.provider.CalendarContract.Events.*

object CalendarUtil {

    fun addEvent(
            context: Context,
            calId: Long,
            title: String,
            description: String,
            startMillis: Long,
            endMillis: Long,
            eventTimeZone: String) {

        val values = ContentValues().apply {
            put(CALENDAR_ID, calId)
            put(TITLE, title)
            put(DESCRIPTION, description)
            put(DTSTART, startMillis)
            put(DTEND, endMillis)
            put(EVENT_TIMEZONE, eventTimeZone)
        }

        val uri = context.contentResolver.insert(CONTENT_URI, values)

        // get the event ID that is the last element in the Uri
        val eventID = uri?.lastPathSegment?.let { java.lang.Long.parseLong(it) }
    }
}
