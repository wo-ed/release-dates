package com.woed.releasedates.utils

import android.app.Activity
import android.content.Context
import android.graphics.PorterDuff
import android.util.SparseBooleanArray
import android.view.Menu
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import org.jetbrains.anko.itemsSequence
import org.jetbrains.anko.toast

fun Activity.threadToast(message: String) {
    runOnUiThread {
        toast(message)
    }
}

fun Activity.threadToast(message: Int) {
    runOnUiThread {
        toast(message)
    }
}

fun Context.getCompatColor(color: Int): Int {
    return ContextCompat.getColor(this, color)
}

fun android.app.Fragment.getCompatColor(color: Int): Int {
    return activity.getCompatColor(color)
}

fun Fragment.getContextColor(id: Int): Int? {
    return context?.let { ContextCompat.getColor(it, id) }
}

fun Menu.setIconsTint(color: Int) {
    itemsSequence()
            .mapNotNull { it.icon }
            .forEach { it.mutate().setColorFilter(color, PorterDuff.Mode.SRC_ATOP) }
}

fun SparseBooleanArray.toList(): List<Pair<Int, Boolean>> {
    return (0 until size()).map { Pair(keyAt(it), valueAt(it)) }
}

fun ImageView.loadUrl(url: String) {
    Glide.with(context).load(url).into(this)
}