package com.woed.releasedates.utils

import android.content.Context
import org.jetbrains.anko.browse

object WebViewUtil {
    fun showGameInfo(restId: Long, context: Context) {
        context.browse("https://www.igdb.com/g/${restId.toString(36)}")
    }
}
