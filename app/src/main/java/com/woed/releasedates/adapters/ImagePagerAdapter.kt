package com.woed.releasedates.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.woed.releasedates.utils.loadUrl

class ImagePagerAdapter(private val context: Context, private val imageUrls: Iterable<String>) :
    PagerAdapter() {

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj
    }

    override fun getCount(): Int = imageUrls.count()

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageUrl = imageUrls.elementAt(position)

        val image = ImageView(context).apply {
            scaleType = ImageView.ScaleType.CENTER_CROP
            loadUrl(imageUrl)
        }
        container.addView(image)
        return image
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as ImageView)
    }
}