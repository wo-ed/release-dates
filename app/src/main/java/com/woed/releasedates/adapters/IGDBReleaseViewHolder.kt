package com.woed.releasedates.adapters

import android.content.Context
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import com.bignerdranch.android.multiselector.MultiSelector
import com.bignerdranch.android.multiselector.MultiSelectorBindingHolder
import com.woed.releasedates.R
import com.woed.releasedates.entities.igdb.IGDBPlatform
import com.woed.releasedates.entities.igdb.IGDBRelease
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick

class IGDBReleaseViewHolder(
    private val context: Context,
    itemView: View,
    private val clickListener: ClickListener,
    multiSelector: MultiSelector,
    private val platforms: Map<Long?, IGDBPlatform>

) : MultiSelectorBindingHolder(itemView, multiSelector) {

    private val dateView: TextView = itemView.find(R.id.textViewDate)
    private val platformView: TextView = itemView.find(R.id.textViewPlatform)
    private val regionView: TextView = itemView.find(R.id.textViewRegion)
    private val checkBoxActivated: CheckBox = itemView.find(R.id.checkBoxActivated)

    private var isSelectable = true

    var release: IGDBRelease? = null

    init {
        itemView.onClick { clickListener.onClick(this) }
    }

    fun bind(release: IGDBRelease?) {
        this.release = release
        val regionText = release?.getRegionText(context)
        regionView.text = regionText
        dateView.text = release?.getDateText(context)
        regionView.visibility = if (regionText != null) View.VISIBLE else View.GONE

        platformView.text = platforms[release?.platformId]?.name
            ?: context.getString(R.string.unknown_platform)
    }

    override fun isSelectable(): Boolean = isSelectable

    override fun setSelectable(isSelectable: Boolean) {
        this.isSelectable = isSelectable
    }

    override fun isActivated(): Boolean = checkBoxActivated.isActivated

    override fun setActivated(isActivated: Boolean) {
        checkBoxActivated.isChecked = isActivated
    }

    interface ClickListener {
        fun onClick(holder: IGDBReleaseViewHolder)
    }
}