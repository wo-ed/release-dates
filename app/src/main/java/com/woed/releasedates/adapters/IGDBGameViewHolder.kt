package com.woed.releasedates.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.woed.releasedates.R
import com.woed.releasedates.entities.igdb.IGDBGame
import com.woed.releasedates.igdb.ImageSize
import com.woed.releasedates.utils.loadUrl
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick

class IGDBGameViewHolder(
    clickListener: ClickListener,
    itemView: View
)

    : RecyclerView.ViewHolder(itemView) {

    private val coverView: ImageView = itemView.find(R.id.imageBackground)
    private val titleView: TextView = itemView.find(R.id.textViewTitle)
    private val platformView: TextView = itemView.find(R.id.textViewPlatform)

    var game: IGDBGame? = null

    init {
        itemView.onClick { clickListener.onClick(this) }
        platformView.visibility = View.GONE
    }

    fun bind(game: IGDBGame?) {
        this.game = game
        titleView.text = game?.name

        game?.cover?.getImageUrl(ImageSize.CoverBig)?.let { coverView.loadUrl(it) }
    }

    interface ClickListener {
        fun onClick(holder: IGDBGameViewHolder)
    }
}