package com.woed.releasedates.adapters

import android.content.Context
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.bignerdranch.android.multiselector.MultiSelector
import com.bignerdranch.android.multiselector.MultiSelectorBindingHolder
import com.woed.releasedates.R
import com.woed.releasedates.entities.Bookmark
import com.woed.releasedates.entities.igdb.IGDBPlatform
import com.woed.releasedates.igdb.ImageSize
import com.woed.releasedates.utils.loadUrl
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import org.jetbrains.anko.onLongClick

class BookmarkViewHolder(
    private val context: Context,
    clickListener: ClickListener,
    itemView: View,
    multiSelector: MultiSelector,
    private val platforms: Map<Long?, IGDBPlatform>
)

    : MultiSelectorBindingHolder(itemView, multiSelector) {

    private var isSelectable = true

    private val coverView: ImageView = itemView.find(R.id.imageBackground)
    private val titleView: TextView = itemView.find(R.id.textViewTitle)
    private val checkBoxActivated: CheckBox = itemView.find(R.id.checkBoxActivated)
    private val platformView: TextView = itemView.find(R.id.textViewPlatform)

    var bookmark: Bookmark? = null

    init {
        itemView.isLongClickable = true
        itemView.onLongClick { clickListener.onLongClick(this) }
        itemView.onClick { clickListener.onClick(this) }
    }

    fun bind(bookmark: Bookmark?) {
        this.bookmark = bookmark

        bindGame(bookmark)
    }

    private fun bindGame(bookmark: Bookmark?) {
        val game = bookmark?.game
        val release = bookmark?.release

        if (game != null && release != null) {
            val platformName = platforms[release.platformId]?.name
                ?: context.getString(R.string.unknown_platform)

            titleView.text = game.name
            platformView.text = platformName

            game.cover?.getImageUrl(ImageSize.CoverBig)?.let { coverView.loadUrl(it) }
        }
    }

    override fun isSelectable(): Boolean = isSelectable

    override fun setSelectable(isSelectable: Boolean) {
        this.isSelectable = isSelectable
    }

    override fun isActivated(): Boolean = checkBoxActivated.isActivated

    override fun setActivated(isActivated: Boolean) {
        checkBoxActivated.isChecked = isActivated
        checkBoxActivated.visibility = if (isActivated) View.VISIBLE else View.INVISIBLE
    }

    interface ClickListener {
        fun onClick(holder: BookmarkViewHolder)
        fun onLongClick(holder: BookmarkViewHolder): Boolean
    }
}