package com.woed.releasedates.adapters

import android.content.Context
import android.view.ViewGroup
import com.bignerdranch.android.multiselector.MultiSelector
import com.woed.releasedates.R
import com.woed.releasedates.entities.Bookmark
import com.woed.releasedates.entities.igdb.IGDBPlatform
import io.realm.OrderedRealmCollection
import io.realm.Realm
import io.realm.RealmRecyclerViewAdapter
import org.jetbrains.anko.layoutInflater

class BookmarkRealmAdapter(
    private val context: Context,
    data: OrderedRealmCollection<Bookmark>,
    autoUpdate: Boolean,
    private val multiSelector: MultiSelector,
    private val clickListener: BookmarkViewHolder.ClickListener
)

    : RealmRecyclerViewAdapter<Bookmark, BookmarkViewHolder>(data, autoUpdate
) {

    private val platforms: Map<Long?, IGDBPlatform> = Realm.getDefaultInstance().use {
        it.where(IGDBPlatform::class.java).findAll().map { platform -> platform.id to platform }.toMap()
    }

    override fun onBindViewHolder(holder: BookmarkViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookmarkViewHolder {
        val view = context.layoutInflater.inflate(R.layout.list_item_bookmark, parent, false)
        return BookmarkViewHolder(context, clickListener, view, multiSelector, platforms)
    }
}
