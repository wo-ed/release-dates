package com.woed.releasedates.adapters

import android.content.Context
import android.view.ViewGroup
import com.bignerdranch.android.multiselector.MultiSelector
import com.woed.releasedates.R
import com.woed.releasedates.entities.igdb.IGDBPlatform
import com.woed.releasedates.entities.igdb.IGDBRelease
import io.realm.OrderedRealmCollection
import io.realm.Realm
import io.realm.RealmRecyclerViewAdapter
import org.jetbrains.anko.layoutInflater

class IGDBReleaseRealmAdapter(
    private val context: Context,
    data: OrderedRealmCollection<IGDBRelease>,
    autoUpdate: Boolean,
    private val clickListener: IGDBReleaseViewHolder.ClickListener,
    private val multiSelector: MultiSelector
)

    : RealmRecyclerViewAdapter<IGDBRelease, IGDBReleaseViewHolder>(
    data, autoUpdate
) {

    private val platforms: Map<Long?, IGDBPlatform> = Realm.getDefaultInstance().use {
        it.where(IGDBPlatform::class.java).findAll().map { it.id to it }.toMap()
    }

    override fun onBindViewHolder(holder: IGDBReleaseViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IGDBReleaseViewHolder {
        val view = context.layoutInflater.inflate(R.layout.list_item_game_release, parent, false)
        return IGDBReleaseViewHolder(context, view, clickListener, multiSelector, platforms)
    }
}
