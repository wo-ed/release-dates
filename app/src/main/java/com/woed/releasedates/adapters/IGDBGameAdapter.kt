package com.woed.releasedates.adapters

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.woed.releasedates.R
import com.woed.releasedates.entities.igdb.IGDBGame
import org.jetbrains.anko.layoutInflater

class IGDBGameAdapter(
    private val context: Context,
    private val games: Iterable<IGDBGame>,
    private val clickListener: IGDBGameViewHolder.ClickListener
)

    : RecyclerView.Adapter<IGDBGameViewHolder>() {

    override fun onBindViewHolder(holder: IGDBGameViewHolder, position: Int) {
        holder.bind(games.elementAt(position))
    }

    override fun getItemCount() = games.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IGDBGameViewHolder {
        val view = context.layoutInflater.inflate(R.layout.list_item_game, parent, false)
        return IGDBGameViewHolder(clickListener, view)
    }
}