package com.woed.releasedates.adapters

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.woed.releasedates.R
import com.woed.releasedates.entities.igdb.IGDBVideo
import org.jetbrains.anko.layoutInflater

class IGDBVideoAdapter(
    private val context: Context,
    private val videos: Iterable<IGDBVideo>,
    private val clickListener: IGDBVideoViewHolder.ClickListener
)

    : RecyclerView.Adapter<IGDBVideoViewHolder>() {

    override fun onBindViewHolder(holder: IGDBVideoViewHolder, position: Int) {
        holder.bind(videos.elementAt(position))
    }

    override fun getItemCount() = videos.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IGDBVideoViewHolder {
        val view = context.layoutInflater.inflate(R.layout.list_item_game, parent, false)
        return IGDBVideoViewHolder(view, clickListener)
    }
}