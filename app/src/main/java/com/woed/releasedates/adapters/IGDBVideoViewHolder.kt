package com.woed.releasedates.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.woed.releasedates.R
import com.woed.releasedates.entities.igdb.IGDBVideo
import com.woed.releasedates.utils.loadUrl
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick

class IGDBVideoViewHolder(
        itemView: View,
        private val clickListener: ClickListener)

    : RecyclerView.ViewHolder(itemView) {

    private val coverView: ImageView = itemView.find(R.id.imageBackground)
    private val titleView: TextView = itemView.find(R.id.textViewTitle)

    var video: IGDBVideo? = null

    init {
        itemView.onClick { clickListener.onClick(this) }
    }

    fun bind(video: IGDBVideo) {
        this.video = video
        titleView.text = video.name
        coverView.loadUrl("https://img.youtube.com/vi/${video.id}/0.jpg")
    }

    interface ClickListener {
        fun onClick(holder: IGDBVideoViewHolder)
    }
}